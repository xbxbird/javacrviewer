package com.xb.web.utils;

import com.crystaldecisions.sdk.occa.report.application.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.data.ConnectionInfo;
import com.crystaldecisions.sdk.occa.report.data.ITable;
import com.crystaldecisions.sdk.occa.report.lib.PropertyBag;
import com.crystaldecisions.sdk.occa.report.lib.PropertyBagHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.jpa.vendor.Database;

/**
 * Created by xb on 10/8/15 AD.
 */
public class CrUtils {
    protected static final Log log = LogFactory.getLog(CrUtils.class);
    static public ReportClientDocument ChangeDataConnection(ReportClientDocument doc,  Database database, String ip, String port, String dbname, String dbservername, String username, String password, String userOrSchemaName) {

        try {

            for (int i = 0; i < doc.getDatabaseController().getDatabase().getTables().size(); i++) {
                ITable table = doc.getDatabaseController().getDatabase().getTables().get(i);

                log.info("processing table : " + table.getName());
                //System.out.println("processing table : " + table.getName());

                String original_qualifiedName = table.getQualifiedName();
                String new_qualifierName = original_qualifiedName;

                switch (database) {
                    case ORACLE:
                        // SCHEMA.TABLENAME
                        String tableName = StringUtils.substringAfterLast(original_qualifiedName, ".").toUpperCase();
                        new_qualifierName = userOrSchemaName.toUpperCase()+"."+tableName;
                        break;
                    case INFORMIX:
                        // DATABASE.OWNER.TABLENAME
                        new_qualifierName = dbname + ":" + userOrSchemaName + "." + StringUtils.substringAfterLast(original_qualifiedName, ".");

                        break;
                    case MYSQL:
                        // DATABASE.OWNER.TABLENAME
                        new_qualifierName = dbname + "." + userOrSchemaName + "." + StringUtils.substringAfterLast(original_qualifiedName, ".");
                        break;
                    default:
                        break;
                }

                String jdbcConnectionString = null;
                String preQEServerName = null;
                String driver = null;

                switch (database) {
                    case INFORMIX:
                        // !com.informix.jdbc.IfxDriver!jdbc:informix-sqli://localhost:40421/export:INFORMIXSERVER=cargool1!user={userid}!password={password}
                        jdbcConnectionString = "!com.informix.jdbc.IfxDriver!jdbc:informix-sqli://"+ip+":"+port+"/"+dbname+":INFORMIXSERVER="+dbservername+"!user={userid}!password={password}";
                        // jdbc:informix-sqli://localhost:40421/export:INFORMIXSERVER=cargool1
                        preQEServerName      = "jdbc:informix-sqli://"+ip+":"+port+"/"+dbname+":INFORMIXSERVER="+dbservername;
                        // com.informix.jdbc.IfxDriver
                        driver = "com.informix.jdbc.IfxDriver";
                        break;
                    case MYSQL:
                        // !com.mysql.jdbc.Driver!jdbc:mysql://172.20.9.170:3306/hego
                        jdbcConnectionString = "!com.mysql.jdbc.Driver!jdbc:mysql://"+ip+":"+port+"/"+dbname;
                        // jdbc:mysql://172.20.9.170:3306/customer
                        preQEServerName      = "jdbc:mysql://"+ip+":"+port+"/"+dbname;
                        // com.mysql.jdbc.Driver
                        driver = "com.mysql.jdbc.Driver";
                        break;
                    case ORACLE:
                        // !oracle.jdbc.driver.OracleDriver!jdbc:oracle:thin:@//localhost:50000/tmsddb2
                        jdbcConnectionString = "!oracle.jdbc.driver.OracleDriver!jdbc:oracle:thin:@//"+ip+":"+port+"/"+dbname;
                        // jdbc:oracle:thin:@//localhost:50000/tmsddb2
                        preQEServerName      = "jdbc:oracle:thin:@//"+ip+":"+port+"/"+dbname;
                        // "oracle.jdbc.driver.OracleDriver"
                        driver = "oracle.jdbc.driver.OracleDriver";
                        break;
                    default:
                        break;
                }


                log.info("Qualfied Name : " + table.getQualifiedName());
                table.setQualifiedName(new_qualifierName);

                ConnectionInfo newConnectionInfo = new ConnectionInfo();

                PropertyBag boPropertyBag1 = new PropertyBag();

                boPropertyBag1.put("JDBC Connection String", jdbcConnectionString);
                boPropertyBag1.put("PreQEServerName",   preQEServerName);
                boPropertyBag1.put("Server Type", "JDBC (JNDI)");
                boPropertyBag1.put(PropertyBagHelper.CONNINFO_CRQE_DATABASETYPE,"Oracle Server");
                boPropertyBag1.put("Database DLL", "crdb_jdbc.dll");
                boPropertyBag1.put("Database", dbname);
                boPropertyBag1.put("Database Class Name", driver);
                boPropertyBag1.put("Use JDBC", "true");
                boPropertyBag1.put("Database Name", dbname);
                boPropertyBag1.put("Server Name", preQEServerName);
                boPropertyBag1.put("Connection URL", preQEServerName);
                boPropertyBag1.put("Server", null);

                newConnectionInfo.setAttributes(boPropertyBag1);
                newConnectionInfo.setUserName(username);
                newConnectionInfo.setPassword(password);

                table.setConnectionInfo(newConnectionInfo);

                doc.getDatabaseController().setTableLocation(table, doc.getDatabaseController().getDatabase().getTables().get(i));
                log.info("Done  : "+ table.getQualifiedName());

            }
            return doc;

        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(ex);
        }

        return doc;

    }


}
