package com.xb.web.controller;

import com.crystaldecisions.sdk.occa.report.application.ParameterFieldController;
import com.crystaldecisions.sdk.occa.report.application.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.lib.ReportSDKException;
import com.xb.web.utils.CrUtils;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
class MainController{

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		return "hello";
	}

	@RequestMapping(value = "/show001", method = RequestMethod.GET)
	 public String showreport001(ModelMap model ,HttpServletRequest request) {

		final String REPORT_NAME = "reports/sample001.rpt";
		try {

			ReportClientDocument reportClientDoc = new ReportClientDocument();
			reportClientDoc.open(REPORT_NAME, 0);

			request.getSession().setAttribute("reportSource", reportClientDoc.getReportSource());

			//Launch CrystalReportViewer page that contains the report viewer.
		}
		catch(ReportSDKException ex) {
//			out.println(ex);
			ex.printStackTrace();
		}
		catch(Exception ex) {
//			out.println(ex);
			ex.printStackTrace();
		}
		return "redirect:/CrystalReportViewer.jsp";
	}

	@RequestMapping(value = "/show002", method = RequestMethod.GET)
	public String showreport002(ModelMap model ,HttpServletRequest request) {

		final String REPORT_NAME = "reports/RPT-ADJ-001.rpt";
		try {

			//Open report.
			ReportClientDocument reportClientDoc = new ReportClientDocument();
			reportClientDoc.open(REPORT_NAME, 0);


			/// Define database connection
			reportClientDoc = CrUtils.ChangeDataConnection(
				reportClientDoc,
				Database.ORACLE,
				"192.168.3.82",	//DB ip address
				"1521",			//DB port
				"orcl",      	//Service name
				"",
				"icom",    		//Username
				"password",		//Password
				"");
			/// Passing parameter
			ParameterFieldController paramFieldController = reportClientDoc.getDataDefController().getParameterFieldController();
			paramFieldController.setCurrentValue("", "V_PERIOD_FROM_ID", new Long("1001"));
			paramFieldController.setCurrentValue("", "V_PERIOD_TO_ID",new Long("1002"));


			//Store the report source in session, will be used by the CrystalReportViewer.
			request.getSession().setAttribute("reportSource", reportClientDoc.getReportSource());

		}
		catch(ReportSDKException ex) {
			ex.printStackTrace();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return "redirect:/CrystalReportViewer.jsp";
	}

}