# README ค่อยๆอ่าน #

This project is demonstration of crystal report with java web application using
 [crystal reportviewer](http://scn.sap.com/docs/DOC-29757)

##Prerequisites##
* Eclipse or other modern java ide
* Crystal report runtime library (included in webapp/WEB-INF/lib )
* Crystal report viewer resource (included in webapp/crystalreportviewers )
* Maven 

##Final sample Project Structure##
    .
    ├── pom.xml
    └── src
        └── main
            ├── java
            │   └── com
            │       └── xb
            │           └── web
            │               ├── controller
            │               │   └── HelloController.java
            │               └── utils
            │                   └── CrUtils.java
            ├── resources
            │   ├── log4j.xml
            │   └── reports
            │       ├── RPT-ADJ-001.rpt
            │       └── sample001.rpt
            └── webapp
                ├── CrystalReportViewer.jsp
                ├── WEB-INF
                │   ├── lib
                │        └─ <list of jar lib>
                │   ├── spring-web-servlet.xml
                │   ├── views
                │   ├── web.xml
                │   └── weblogic.xml
                ├── crystalreportviewers
                │   ├── <list of crystal report viewer resource>
                │   

## Need to do following steps to implement ##

### 1. Add library to POM dependency ###
See [POM](https://bitbucket.org/xbxbird/javacrviewer/src/1324e3098432/pom.xml)    
    
**Remark**
> Some library does not been maven provided is include (`WEB-INF/lib`)
> that's why POM has `<scope>system</scope>` (Remove it if install to local repository)

### 2. Add `/crystalreportviewers/` to `/webapp/` ###
**Remark**
> The `/crystalreportviewers/` should be place in same level as `CrystalReportViewer.jsp`
![Basic](http://s11.postimg.org/wzmnash4j/Screen_Shot_2558_10_09_at_8_04_54_PM.png)

### 3. Add RPT files ###
In this project .rpt place in `/src/main/resouces/reports`

### 4. Add context and servlet to `web.xml` ###

```xml
<context-param>
	<param-name>crystal_image_uri</param-name>
	<param-value>crystalreportviewers</param-value>
</context-param>

<context-param>
	<param-name>crystal_image_use_relative</param-name>
	<param-value>webapp</param-value>
</context-param>

<servlet>
	<servlet-name>CrystalReportViewerServlet</servlet-name>
	<servlet-class>com.crystaldecisions.report.web.viewer.CrystalReportViewerServlet</servlet-class>
	<!--<load-on-startup>3</load-on-startup>-->
</servlet>

<servlet-mapping>
	<servlet-name>CrystalReportViewerServlet</servlet-name>
	<url-pattern>/CrystalReportViewerHandler</url-pattern>
</servlet-mapping>
```

### 5. Write Controller/Action  ###

#### 5.1 Open report####
```java
final String REPORT_NAME = "reports/REPORTNAME.rpt";
ReportClientDocument reportClientDoc = new ReportClientDocument();
reportClientDoc.open(REPORT_NAME, 0);
```
#### 5.2 Change database connection####
```java
reportClientDoc = CrUtils.ChangeDataConnection(
	reportClientDoc,
	Database.ORACLE,	//Select db provider
	$DBHOST,	//DB ip address
	$DBPORT",			//DB port
	$DBService,      	//Service name
	"",				//not use
	"user",    		//Username
	"pass",			//Password
	"");			//not use
```

#### 5.3 Pass report's parameter (If it there are)####
```java
/// Passing parameter
ParameterFieldController paramFieldController = reportClientDoc.getDataDefController().getParameterFieldController();
paramFieldController.setCurrentValue("", "V_PERIOD_FROM_ID", new Long("1001"));
paramFieldController.setCurrentValue("", "V_PERIOD_TO_ID",new Long("1002"));
```
#### 5.4 Store ReportClientDocument in Session####
```java
//Store the report source in session, will be used by the CrystalReportViewer.
request.getSession().setAttribute("reportSource", reportClientDoc.getReportSource());
```
#### 5.5 Redirect to `/CrystalReportViewer.jsp`####
```java
return "redirect:/CrystalReportViewer.jsp";
```

## Example screen ##
### home screen ###
![home](http://s17.postimg.org/99yd4f6bj/Screen_Shot_2558_10_09_at_7_22_34_PM.png
)    
### basic report ###
![Basic](http://s24.postimg.org/81qafpi11/Screen_Shot_2558_10_09_at_7_22_53_PM.png)    
### second report ###
![Complex]
(http://s17.postimg.org/st2yds533/Screen_Shot_2558_10_09_at_7_23_29_PM.png)   
### What is this repository for? ###